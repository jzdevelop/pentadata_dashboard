// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBcZHiaO4l9J_cRcDMXqMoAbgB2XKSWDNA",
    authDomain: "smet-d8c83.firebaseapp.com",
    databaseURL: "https://smet-d8c83.firebaseio.com",
    projectId: "smet-d8c83",
    storageBucket: "smet-d8c83.appspot.com",
    messagingSenderId: "895530049495",
    appId: "1:895530049495:web:9294940224aead27"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
