import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'charts',
    loadChildren: './pages/charts/charts.module#ChartsModule'
  },
  {
    path: 'login',
    loadChildren: './pages/login/login.module#LoginModule'
  },
  {
    path: 'sections',
    loadChildren: './pages/sections/sections.module#SectionsModule'
  },
  {
    path: 'salesplaces',
    loadChildren: './pages/sales-places/sales-places.module#SalesPlacesModule'
  },
  {
    path: 'locations',
    loadChildren: './pages/maps/maps.module#MapsModule'
  },
  {
    path: 'sensors',
    loadChildren: './pages/sensors/sensors.module#SensorsModule'
  },
  {
    path: 'fridges',
    loadChildren: './pages/fridges/fridges.module#FridgesModule'
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
