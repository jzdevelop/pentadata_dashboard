import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  alert = '';
  email = '';
  password = '';
  constructor(private router: Router) { }

  ngOnInit() {
  }

  login(){
    this.email = (<HTMLInputElement>document.getElementById('email')).value;
    this.password = (<HTMLInputElement>document.getElementById('password')).value;
    if (this.email === 'admin@correo.com' && this.password === 'admin') {
      this.router.navigate(['/sections']);
    }
    else{
      this.alert = 'Usuario y/o contraseña incorrectos';
    }
  }

}
