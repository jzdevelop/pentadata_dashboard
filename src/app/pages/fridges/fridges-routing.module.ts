import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FridgesComponent } from './fridges.component';


const routes: Routes = [
  {
    path: '',
    component: FridgesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FridgesRoutingModule { }
