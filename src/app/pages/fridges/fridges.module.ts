import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FridgesRoutingModule } from './fridges-routing.module';
import { FridgesComponent } from './fridges.component';


@NgModule({
  declarations: [FridgesComponent],
  imports: [
    CommonModule,
    FridgesRoutingModule
  ]
})
export class FridgesModule { }
