import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import 'firebase/firestore';
import { environment } from 'src/environments/environment';
import * as moment from 'moment-timezone';
import { Chart } from 'chart.js';
import {GetDataFromDbService} from '../../services/get-data-from-db.service';
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

  energyMeasured: any[] = [];
  dateSeries: any[] = [];
  temperatureMeasured: any[] = [];
  temperatureDate: any[] = [];
  db: any;
  constructor(private getDataFromDb: GetDataFromDbService) {
  }

  ngOnInit() {
    this.getDataFromFirebase(this.getDate());
    //this.drawChart(dataEnergyMeasured:any);
  }

  getDataFromFirebase(date: any) {
    //firebase.initializeApp(environment.firebaseConfig);
    //const db = firebase.firestore();
    this.getDataFromDb.db.collection('EnergyDB').where('date', '>=', date[0]).where('date', '<=', date[1]).where('id', '==', '0').orderBy('date', 'asc').get().then((querySnapshot) => {
      querySnapshot.forEach((doc: any) => {
        //console.log("data: " + doc.data());
        this.energyMeasured.push(doc.data().power);
        this.temperatureMeasured.push(doc.data().temperature);
        this.dateSeries.push(doc.data().date);
        //console.log(date);
      });
      this.drawChart();
    });
  }
  getDate() {
    const date = moment().tz('America/Bogota').format();
    const year = date.slice(2, 4);
    //console.log('year' + year);
    const month = date.slice(5, 7);
    const day = date.slice(8, 10);
    //console.log("day" + day);
    const hour = date.slice(11, 13);
    const minutes = date.slice(14, 16);

    // get date now without minutes and get date with last hour, to get the last one hour range and return as array
    //const dateNow = year.toString() + month.toString() + day.toString()  + hour.toString() + '00';
    const lastHour = parseInt(hour) - 1;
    let auxLastHour = "" + lastHour;
    if (hour === '00') {
      auxLastHour = '23';
    }
    else if (hour === '01') {
      auxLastHour = '00';
    }
    else if (lastHour <= 9) {
      auxLastHour = '0' + lastHour;
    }
    const dateLastHour = '' + year  + month + day + auxLastHour + '00';
    //console.log('datelasthour: ' + dateLastHour);
    const dateNow = '' + year + month + day + hour + '00';
    let dateInNumber: any[] = []
    dateInNumber.push(parseInt(dateLastHour));
    dateInNumber.push(parseInt(dateNow));
    //console.log('date: ' + dateInNumber);
    return dateInNumber;
  }

  drawChart() {
    const chartId = document.getElementById('myChart');
    const chart = new Chart(chartId, {
      // The type of chart we want to create
      type: 'bar',

      // The data for our dataset
      data: {
        labels: this.dateSeries,
        datasets: [{
          label: 'Temperatura (°C)',
          yAxisID: 'Temperature',
          backgroundColor: '#01579b',
          borderColor: '#01579b',
          fill: false,
          data: this.temperatureMeasured,
          type: 'line'
        },
        {
          label: 'Consumo de energía (KW)',
          yAxisID: 'Power',
          backgroundColor: '#80cbc4',
          borderColor: '#80cbc4',
          data: this.energyMeasured,
          type: 'bar'
        }]
      },

      // Configuration options go here
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Temperatura (°C)',
              fontStyle: 'bold',
              fontSize: 14
            },
            ticks: {
              beginAtZero: true
            },
            id: 'Temperature',
            type: 'linear',
            position: 'left'
          },
          {
            scaleLabel: {
              display: true,
              labelString: 'Energía (KW)',
              fontStyle: 'bold',
              fontSize: 14
            },
            ticks: {
              beginAtZero: true
            },
            id: 'Power',
            type: 'linear',
            position: 'right'
          }
          ]
        }
      }
    });
  }

}
