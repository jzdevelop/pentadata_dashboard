import { Component, OnInit } from '@angular/core';
import {GetDataFromDbService} from '../../services/get-data-from-db.service';
declare var google;
@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {

  locations: any[] = [];
  constructor(private getDataFromDb: GetDataFromDbService ) { }

  ngOnInit() {
    this.getDataFromFirestore();

  }
  loadMap(locations: any[]) {
    const map = new google.maps.Map(document.getElementById('map'), {
      center: new google.maps.LatLng(locations[0].latitude, locations[0].longitude),
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    const infowindow = new google.maps.InfoWindow();
    let marker;
    let i;

    for (i = 0; i < this.locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i].latitude, locations[i].longitude),
        map: map
      });

      google.maps.event.addListener(marker, 'click', ((marker: any, i: any)=> {
        return () => {
          infowindow.setContent(locations[i].name);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }

  }
  getDataFromFirestore(){
    this.getDataFromDb.db.collection('FrancorpSalesPlaces').get().then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        this.locations.push({name: doc.data().name, latitude: doc.data().latitude, longitude: doc.data().longitude });
      });
      this.loadMap(this.locations);
    });
  }


}
