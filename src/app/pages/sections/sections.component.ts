import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {GetDataFromDbService} from '../../services/get-data-from-db.service';
@Component({
  selector: 'app-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.css']
})
export class SectionsComponent implements OnInit {

  constructor(private router: Router, private getDataFromDb: GetDataFromDbService) { }

  energy = 0.0;
  ngOnInit() {
    this.getDataFromFirestore();
  }

  salesPlaces(){
    this.router.navigate(['/salesplaces']);
  }
  locations(){
    this.router.navigate(['/locations']);
  }

  sensors(){
    this.router.navigate(['/sensors']);
  }

  fridges(){
    this.router.navigate(['/fridges']);
  }

  getDataFromFirestore(){
    this.getDataFromDb.db.collection('EnergyDB').orderBy('date', 'desc').limit(1).get().then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        this.energy = doc.data().power.toFixed(2);
      });
    });
  }

}
