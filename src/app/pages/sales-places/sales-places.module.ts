import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SalesPlacesComponent} from './sales-places.component';
import { SalesPlacesRoutingModule } from './sales-places-routing.module';


@NgModule({
  declarations: [SalesPlacesComponent],
  imports: [
    CommonModule,
    SalesPlacesRoutingModule
  ]
})
export class SalesPlacesModule { }
