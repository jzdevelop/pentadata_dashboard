import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPlacesComponent } from './sales-places.component';

describe('SalesPlacesComponent', () => {
  let component: SalesPlacesComponent;
  let fixture: ComponentFixture<SalesPlacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesPlacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPlacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
