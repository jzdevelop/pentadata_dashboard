import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {GetDataFromDbService} from '../../services/get-data-from-db.service';
@Component({
  selector: 'app-sales-places',
  templateUrl: './sales-places.component.html',
  styleUrls: ['./sales-places.component.css']
})
export class SalesPlacesComponent implements OnInit {

  constructor(private router: Router, private getDataFromDb: GetDataFromDbService) { }

  temperature = 0.0;
  energy = 0.0;
  ngOnInit() {
    this.getDataFromFirestore();
  }

  navigateToChart(){
    this.router.navigate(['/charts']);
  }

  getDataFromFirestore(){
    this.getDataFromDb.db.collection('EnergyDB').orderBy('date', 'desc').limit(1).get().then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        this.energy = doc.data().power.toFixed(2);
        this.temperature = doc.data().temperature.toFixed(2);
      });
    });
  }

}
