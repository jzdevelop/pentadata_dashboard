import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesPlacesComponent } from './sales-places.component';


const routes: Routes = [
  {
    path: '',
    component: SalesPlacesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesPlacesRoutingModule { }
