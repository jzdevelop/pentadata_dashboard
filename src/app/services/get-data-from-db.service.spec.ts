import { TestBed } from '@angular/core/testing';

import { GetDataFromDbService } from './get-data-from-db.service';

describe('GetDataFromDbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetDataFromDbService = TestBed.get(GetDataFromDbService);
    expect(service).toBeTruthy();
  });
});
