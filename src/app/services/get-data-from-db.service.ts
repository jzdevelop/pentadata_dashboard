import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import 'firebase/firestore';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class GetDataFromDbService {

  public db: any;
  constructor() {
    firebase.initializeApp(environment.firebaseConfig);
    this.db = firebase.firestore();
  }

}
